const { 
  ASYNC_WEBTRIGGER_URL,
  SLACK_OAUTH_BEARER_TOKEN,
  DEBUG_LOGGING
} = process.env;

const errors = [];

if (!ASYNC_WEBTRIGGER_URL) {
  errors.push('ASYNC_WEBTRIGGER_URL must be set to the webtrigger url for the webtrigger module with key "async-webtrigger"');
}

if (!SLACK_OAUTH_BEARER_TOKEN) {
  errors.push('SLACK_OAUTH_BEARER_TOKEN must be set to the OAuth bearer token for your Slack app')
}

if (errors.length > 0) {
  console.error(
    `${errors.length} environment variables are missing or misconfigured:` + 
    `${errors.join('\n ')}` +
    `Please see README.md for further details.`
  );
  throw new Error('Environment variable config error');
}

export const asyncWebTriggerUrl = ASYNC_WEBTRIGGER_URL;
export const slackOAuthBearerToken = SLACK_OAUTH_BEARER_TOKEN;
export const debugLogging = DEBUG_LOGGING && true;
