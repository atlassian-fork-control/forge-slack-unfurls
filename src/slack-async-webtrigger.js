import { fetchIssue } from './client/jira';
import { apiRequest as slackRequest } from './client/slack';
import { warn } from './util/logger';

export async function handleSlackWebhookAsync(request) {
  let { body, headers, query } = request;
  body = JSON.parse(body);

  // https://*.atlassian.net/browse/* link(s) shared in a Slack message
  if (body.event && body.event.type === 'link_shared') {
    await onLinkShared(body.event);
    return;
  }

  warn(`Unrecognized request sent to slack-async-webtrigger: ${JSON.stringify(request)}`);
}

async function onLinkShared(slackEvent) {
  // `link_shared` events contain the links shared in a Slack message
  const attachments = slackEvent.links.map(async (link) => {
    // Extract the issue key from the link URL
    const issueKey = link.url.substring(link.url.lastIndexOf('/') + 1);
    const issue = await fetchIssue(issueKey);
    // Format issue preview as a Slack unfurl
    const unfurl = {};
    unfurl[link.url] = formatIssueAttachment(issue, link.url);
    return unfurl;
  });

  const mergedUnfurls = (await Promise.all(attachments)).reduce((unfurls, unfurl) => {
    return Object.assign(unfurls, unfurl);
  }, {});

  return slackRequest('chat.unfurl', {
    channel: slackEvent.channel,
    ts: slackEvent.message_ts,
    unfurls: mergedUnfurls
  });
}

function formatIssueAttachment(issue, link) {
  const title = `${issue.key}: ${issue.fields.summary}`;
  return {
      fallback: title,
      color: "#0052CC",
      author_name: issue.fields.reporter.displayName,
      author_icon: `${issue.fields.reporter.avatarUrls['48x48']}&format=png`,
      title: title,
      title_link: link,
      fields: [
          {
              title: "Priority",
              value: issue.fields.priority.name,
              short: true
          },
          {
              title: "Status",
              value: issue.fields.status.name,
              short: true
          }
      ],
      thumb_url: `${issue.fields.issuetype.iconUrl.replace('size=xsmall', 'size=xlarge')}&format=png`,
      footer: issue.fields.project.name,
      footer_icon: `${issue.fields.project.avatarUrls['48x48']}&format=png`,
  };
}