import { asyncWebTriggerUrl } from './config'
import { sleep, nowMs } from './util/time';
import { info, warn, debug } from './util/logger';

export async function handleSlackWebhook(request) {
  let { body, headers, query } = request;
  body = JSON.parse(body);

  // Webhook registration challenge from Slack
  if (body.challenge) {    
    info("Received Event Subscription challenge from Slack!")
    const challengeResponse = {
      body: body.challenge,
      headers: {'Content-Type': ['text/plain']},
      statusCode: 200
    };
    // Return value is used as response to the trigger request
    return challengeResponse;
  }

  // https://*.atlassian.net/browse/* link(s) shared in a Slack message
  if (body.event && body.event.type === 'link_shared') {
    await dispatchToAsyncWebTrigger(request);
    return {
      statusCode: 204
    };
  }

  warn(`Unrecognized request sent to slack-webtrigger: ${JSON.stringify(request)}`);
}

/**
 * Slack event subscription webhooks require the server to respond in less 
 * than three seconds, or it considers the request a failure and performs 
 * a retry (https://api.slack.com/events-api#failure_conditions).
 * 
 * In order to respond quickly, we asynchronously dispatch the request to 
 * a web trigger to render the issue preview. This is a bit of a hack, until
 * Forge supports directly triggering functions asynchronously.
 */
async function dispatchToAsyncWebTrigger(request) {
  const { headers, method, body } = request;
  debug('Dispatching event to slack-async-webtrigger for processing');
  api.fetch(asyncWebTriggerUrl, { headers, method, body });
  await sleep(1000);
}
