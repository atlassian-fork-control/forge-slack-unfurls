import { debug } from './logger';

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function nowMs() {
  return new Date().getTime();
}

export async function timer(label, fn) {
  const startMs = nowMs();
  try {
    return await fn();
  } finally {
    debug(`${label} finished in ${nowMs() - startMs}ms`);
  }
}