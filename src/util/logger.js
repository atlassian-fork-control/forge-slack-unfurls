import { debugLogging } from '../config';

export function error() {
  console.error.apply(this, arguments);
}

export function warn() {
  console.warn.apply(this, arguments);
}

export function info() {
  console.log.apply(this, arguments);
}

export function debug() {
  if (debugLogging) {
    console.debug.apply(this, arguments);
  }
}

export function trace() {
  if (debugLogging) {
    console.trace.apply(this, arguments);
  }
}
